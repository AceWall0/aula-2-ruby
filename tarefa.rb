require 'date'

# Módulo para ajudar na exibição das classes.
module Tab
    extend self
    @@level = 0
    def to_s
        return " " * 4 * @@level
    end
    def begin
        @@level += 1
    end
    def end
        @@level -= 1
    end
end


class Usuario
    attr_accessor :email, :nome, :nascimento

    def initialize(email, senha, nome, nascimento)
        raise TypeError, "Nascimento não é do tipo Date" unless nascimento.is_a? Date
        
        @email = email
        @senha = senha
        @nome = nome
        @logado = false
        @nascimento = nascimento
    end
        
    def idade
        return (Date.today - @nascimento).to_i / 365
    end

    def logado?
        @logado
    end

    def logar(senha)
        if senha == @senha
            @logado = true
        end
    end

    def deslogar
        @logado = false
    end
end


class Aluno < Usuario
    attr_reader :matricula, :curso, :turmas
    
    def initialize(email:, senha:, nome:, nascimento:, matricula:, periodo_letivo:, curso:, turma:)
        raise TypeError, "Turma não é do tipo turma" unless turma.is_a? Turma
        super(email, senha, nome, nascimento)
        @matricula = matricula
        @periodo = periodo_letivo
        @curso = curso
        @turmas = []
        turma.adicionar_aluno(self)
    end

    def inscrever(turma)
        raise TypeError, "Turma não é do tipo turma" unless turma.is_a? Turma
        @turmas.push(turma) 
    end

    def to_s
        str = "Aluno(\n"
        Tab.begin
        str += "#{Tab}nome: '#{@nome}',\n"
        str += "#{Tab}curso: '#{@curso}'\n"
        Tab.end
        str += "#{Tab})\n"
    end
end


class Professor < Usuario
    attr_accessor :turma
    attr_reader :matricula, :salario, :materias
    
    def initialize(email:, senha:, nome:, nascimento:, matricula:, salario:, materia:)
        super(email, senha, nome, nascimento)
        @matricula = matricula
        @salario = salario
        @materias = [materia]
        @turma = []
    end

    def adicionar_materia(materia)
        # O unless previne que materia adicione professor e professor adicione materia infinitamente.
        unless materias.include?(materia)
            @materias.push(materia)
            materia.adicionar_professor(this)
        end
    end

    def to_s
        str = "Professor\n"
        Tab.begin
        str += "#{Tab}nome: '#{@nome}',\n"
        str += "#{Tab}matérias: '#{@materias}'\n"
        Tab.end
        str += ")\n"
    end
end


class Turma 
    attr_accessor :nome, :horario, :dias_da_semana, :professor
    attr_reader :inscritos

    def initialize(nome:, horario:, dias_da_semana:, professor:)
        @nome = nome
        @horario = horario
        @dias_da_semana = dias_da_semana
        @professor = professor
        @inscricao_aberta = false
        @inscritos = []
        @professor.turmas.push(self)
    end

    def abrir_inscricao
        @inscricao_aberta = true
    end

    def fechr_inscricao
        @inscricao_aberta = false 
    end

    def inscricao_aberta? 
        @inscricao_aberta
    end

    def adicionar_aluno(aluno)
        if inscricao_aberta?
            inscritos.push(aluno)
            aluno.inscrever(self)
        else
            puts "Não foi possível adicionar aluno. As inscrições estão fechadas."
        end
    end

    def to_s 
        str = "Turma(\n"
        Tab.begin
        str += "#{Tab}nome: '#{@nome}',\n"
        str += "#{Tab}inscritos: [\n"
        Tab.begin
        @inscritos.each {|e| str += "#{Tab}#{e}"}
        Tab.end
        str += "#{Tab}]\n"
        Tab.end
        str += "#{Tab})"
        return str
    end
end


=begin
Apesar de no documento dizer que uma matéria precisa ter no mínimo 1 professor,
eu lembro de ter sido dito da possibilidade de existir matérias em que não existe professor.

Por esse motivo, e por ficar mais fácil para implementar e para usar a classe, eu vou
assumir que a cardinalidade de Materia -> Professor seja 0..n
=end
class Materia
    attr_accessor :nome, :ementa
    attr_reader :professores
    def initialize(nome, ementa)
        @nome = nome 
        @ementa = ementa
        @professores = []
    end
    
    def adicionar_professor(professor)
        unless professores.include?(professor)
            @professores.push(professor)
            professor.adicionar_materia(this)
        end
    end
end



# ==================  Testes e afins ==========================
test_professor = Professor.new(
    email: 'asd@rew', 
    senha: 'segura', 
    nome: 'gerivaldo',
    nascimento: Date.new(2030, 3, 2), 
    matricula: '123.345.432',
    salario: '1000000000',
    materia: Materia.new('Jediismo', 'Ee asd oo iipppo dasd')
)

test_turma = Turma.new(nome: 'asd', horario: 'asd', dias_da_semana: 'asd', professor: test_professor)
test_turma.abrir_inscricao

test_aluno = Aluno.new(
    email: 'asd@asd',
    senha: 12345,
    nome: 'Lorem',
    nascimento: Date.new(1994, 6, 17),
    matricula: '123.345',
    periodo_letivo: 5,
    curso: 'Ipsum',
    turma: test_turma
)
